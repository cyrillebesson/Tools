# 💻 Hardware

- MacBook Air 13-inch (M1, 2020)
- USB C to USB / HDMI / USB C dongle

# ✅ Apps

## 🌍 Web

- Brave Broswer
- Firefox Developer Edition
- Tor Browser

## 🔒 Privacy

- Bitwarden
- Authy

## 💬 Communication

- Protonmail (PWA)
- Signal
- Telegram (PWA)

## 🖍 Design

- Figma
- Affinity Designer
- Krita

## 💾 Dev

- Visual Studio Code
- Github Desktop App

## 💎 Crypto

- Ledger Live
- Binance (PWA)

## 👥 Social (PWA)

- Twitter
- Linkedin
- Reddit

## 🛠 Productivity

- Libre Office
- Alfred
- Amphetamine
- Spectacle
- Barrier
- CheatSheet
- App Cleaner

## 🎞 Media
- VLC
- Stremio
